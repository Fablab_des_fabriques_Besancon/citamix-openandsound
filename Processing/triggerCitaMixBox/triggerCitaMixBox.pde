import processing.serial.*;
import ddf.minim.*;

Serial myPort;                  // Le port série

short LF = 10; // "Fin de ligne"

char HEADER_0 = '0'; 
char HEADER_1 = '1'; 
char HEADER_2 = '2'; 

Minim minim;

AudioPlayer audio0;
AudioPlayer audio1;
AudioPlayer audio2;

boolean firstContact = false;  

boolean[] buttonState = {false, false, false};
int offset;

void setup()
{
  size(512, 200, P3D);
  minim = new Minim(this);

  // load BD.wav from the data folder
  audio0 = minim.loadFile( "Piste1.wav", 512);
  audio1 = minim.loadFile( "Piste2.wav", 512);
  audio2 = minim.loadFile( "Piste3.wav", 512);

  // Nous imprimons la liste des ports série disponibles
  printArray(Serial.list());
  // Puis nous allons choisir le premier du tableau (à adapter suivant votre configuration), avant de nous connecter à 9600 baud (même vitesse que l'Arduino).
  String portName = Serial.list()[0];
  myPort = new Serial(this, portName, 9600);

  offset = width/3;
  ellipseMode(CENTER);
}

void draw()
{
  background(0);
  stroke(255);

  //Some visual helpers
  for (int i = 0; i<3; i++)
  {
    if (buttonState[i] == true)
    {
      fill(255, 0, 0);
    } else {
      fill(20, 20, 20);
    }
    ellipse(offset/2+offset*i, height/2, 40, 40);
  }
}

void serialEvent(Serial myPort) {
  String message = myPort.readStringUntil(LF); // On lit le message reçu, jusqu'au saut de ligne
  if (message != null)
  {
    print("Message " + message);
    // On découpe le message à chaque virgule, on le stocke dans un tableau
    String [] data = message.split(",");

    if (firstContact == false) {
      if (data[0].charAt(0) == 'A') {
        myPort.clear();          // clear the serial port buffer
        firstContact = true;     // you've had first contact from the microcontroller
        myPort.write('A');       // ask for more
      }
    } else {


      if (data[0].charAt(0) == HEADER_0)
      {
        // On convertit la valeur (String -> Int)
        int Btn1 = Integer.parseInt(data[1]);
        if (Btn1 == 1)
        {
          audio0.rewind();
          audio0.loop();
          buttonState[0] = true;
        } else {
          audio0.pause();
          buttonState[0] = false;
        }
      } else if (data[0].charAt(0) == HEADER_1)
      {
        // On convertit la valeur (String -> Int)
        int Btn2 = Integer.parseInt(data[1]);
        if (Btn2 == 1)
        {
          audio1.rewind();
          audio1.loop();
          buttonState[1] = true;
        } else {
          audio1.pause();
          buttonState[1] = false;
        }
      } else if (data[0].charAt(0) == HEADER_2)
      {
        // On convertit la valeur (String -> Int)
        int Btn3 = Integer.parseInt(data[1]);
        if (Btn3 == 1)
        {
          audio2.rewind();
          audio2.loop();
          buttonState[2] = true;
        } else {
          audio2.pause();
          buttonState[2] = false;
        }
      }
    }
  }
}


void keyPressed() 
{
  if ( key == 'a' ) audio0.play();
  if ( key == 'z' ) audio1.play();
  if ( key == 'e' ) audio2.play();
}
