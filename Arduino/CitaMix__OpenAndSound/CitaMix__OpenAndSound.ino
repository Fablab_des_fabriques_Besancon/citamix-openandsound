boolean buttonState[3] = {LOW, LOW, LOW};
boolean previousButtonState[3] = {LOW, LOW, LOW};
int score[5] = {0, 0, 0};

byte Pins[] = {A0, A1, A2};

// Adust, lower is more sensitive
int seuil = 10;
int trig = 500;
int scoreMax = 1000;

void setup()
{
  pinMode(A0, INPUT);
  pinMode(A1, INPUT);
  pinMode(A2, INPUT);

  Serial.begin(9600);

  establishContact();  // send a byte to establish contact until receiver responds

}

void establishContact() {
  while (Serial.available() <= 0) {
    Serial.println('A');   // send a capital A
    delay(300);
  }
}

void loop()
{
  // if (Serial.available() > 0) {
  // Grab values
  for (int i = 0; i < 3; i++)
  {
    int readedSensor = analogRead(Pins[i]);
    //Serial.print(readedSensor);
    //Serial.print('\t');
    if (readedSensor > seuil) // somebody touched
    {
      if (score[i] < scoreMax)score[i] += 400;

    }
  }
  //Serial.println();

  //Smoothing values
  for (int i = 0; i < 3; i++)
  {
    score[i] = (2 * score[i]) / 3;
    //Serial.print(score[i]);
    //Serial.print('\t');
  }
  //Serial.println();



  // Set Button State
  for (int i = 0; i < 3; i++)
  {
    if ( score[i] > trig)// Front montant
    {
      if (buttonState[i] == LOW)
      {
        buttonState[i] = HIGH;
        previousButtonState[i] = LOW;
      }
    }
    else if ( score[i] < trig)// Front descendant
    {
      if  ( buttonState[i] == HIGH)
      {
        buttonState[i] = LOW;
        previousButtonState[i] = HIGH;
      }
    }
    // Sending to processing only if state has changed
    if (buttonState[i] != previousButtonState[i])
    {
      Serial.print(i);
      Serial.print(",");
      Serial.print(buttonState[i], DEC);
      Serial.print(",");
      Serial.println();
      previousButtonState[i] = buttonState[i];
    }
  }


  delay(10);
}
