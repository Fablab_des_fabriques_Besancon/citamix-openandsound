**Programme réalisé dans le cadre de Citamix 2020**

-

Le projet consiste à créer 3 capteurs de luminosité cachés dans des boites en bois.L'ouverture d'un couvercle déclenche la lecture d'un son via un programme Processing. Celui gère la lecture de différentes pistes audio via la bibliothèque Minim. Le support a été modélisé avec Freecad et imprimé sur une Ultimaker 3.


![](Pics/DSC00549.JPG)

![](Pics/DSC00546.JPG)